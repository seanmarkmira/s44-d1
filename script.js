// Get post data
// Promise - Holds the eventual result of an asynchronous operation
// Promise states - pending, resolved or fulfilled, rejected
// Asynchronous Operation - Any JS operation that would take time to complete. Example would be networks request
let entriesTitle = document.getElementById("div-post-entries")
//How to consume promises
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=> response.json())
.then(data=> showPosts(data))
.catch(err => console.log(err))

const showPosts = (posts)=>{
	let postEntries = ''
	posts.forEach((post)=>{
		postEntries += `
		<div id="post-${post.id}">
		<h3 id="post-title-${post.id}">${post.title}</h3>
		<p id="post-body-${post.id}">${post.body}</p>
		<button onclick="editPost(${post.id})">Edit</button>
		<button onclick="deletePost(${post.id})">Delete</button>
		</div>
		<hr>
		`
	})
	entriesTitle.innerHTML = postEntries
}

// Edit Post
const editPost = (id) => {
	//console.log(`This is edit post id: ${id}`)
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;
	document.querySelector('#txt-edit-title').value=title;
	document.querySelector('#txt-edit-body').value=body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
	document.querySelector('#txt-edit-id').value=id;
	
	//Update
	let updateForm = document.querySelector('#form-edit-post')
	console.log(updateForm)
	updateForm.addEventListener('submit', (e)=>{
		e.preventDefault(false);
		//fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, ...
		fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'PUT',
			body: JSON.stringify({
				id: id,
				title: document.querySelector('#txt-edit-title').value,
				body: document.querySelector('#txt-edit-body').value,
				userId:1
			}),
			headers:{'content-type': 'application/json; charset=UTF-8'}
		})
		.then(response => response.json())
		.then(data=>{
			console.log(data)
		})
	})
}

// Delete Post
const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, { method: 'DELETE' });
    document.querySelector(`#post-${id}`).remove();
}

//Add post
let addPostForm = document.querySelector('#form-add-post')
addPostForm.addEventListener("submit",(e)=>{
	console.log('Submit has been triggered')
	
	//stops the whole page to refresh upon submition of form
	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts',{
		method:'POST',
		body: JSON.stringify({
			title:document.getElementById("txt-title").value,
			body:document.getElementById("txt-body").value,
			userId: 1
		}),
		headers:{'Content-type': 'application/json; charset=UTF-8'}
	})
	.then(response => response.json())
	.then(data=>{ 
		console.log(data)
		document.querySelector('#txt-title').value=""
		document.querySelector('#txt-body').value=""
		alert('Post Added')
		//We have to make the title and body to be blank after sending post
		
		//
	})
})

// Anatomy of a promise
// const p = new Promise((resolve, reject)=>{
// 	// resolve(1)
// 	reject(new Error('message'))
// })
// 
// // console.log(p)
// p.then(result=> console.log(result)).catch(error => console.log(error))
